# Luban

#### 项目介绍

- 项目名称：Luban
- 所属系列：openharmony的第三方组件适配移植
- 功能：OHOS的图像压缩工具，效率非常接近微信时刻。此模块有助于压缩类型为：png、jpg、位图和gif的图像。
- 项目移植状态：主功能完成
- 调用差异：无
- 基线版本：turbo-1.0.0
- 开发版本：SDK ver 2.1.0.13，IDE Ver 2.1.1.201004

#### 安装教程

1.对于在示例应用程序中使用Luban模块，请包含以下库依赖以生成hap/library.har。
修改条目build.gradle，如下所示：

```
dependencies {
    implementation project(path: ':library')
}
```

2.对于在单独的应用程序中使用Luban，请确保在“条目”模块的libs文件夹中添加“library.har”。
修改条目build.gradle，如下所示：

```
dependencies {
    implementation fileTree(dir: 'libs', include: ['*.har'])
}
```

3.要在单独的应用程序中使用远程存储库的Luban，请在“条目”build.gradle中添加以下依赖项。
修改条目build.gradle，如下所示：

```
dependencies {
  implementation 'io.openharmony.tpc.thirdlib:luban:1.0.0'
}
```

#### 使用说明

1.Luban内部使用IO线程执行图像压缩，实现只需要指定进程成功完成时发生的事情。

```java
Luban.with(this)
	.setTargetDir(/*PATH*/) // pass target data directory path
	.ignoreBy(/*SIZE*/)		// pass least compress size
	.load(/*FILE*/) 		// pass image to be compressed
	.setCompressListener(new OnCompressListener() { // Set up return
		@Override
		public void onStart() {
			// TODO Called when compression starts, display loading UI here
		}

	@Override
	public void onSuccess(File file) {
		// TODO Called when compression finishes successfully, provides compressed image
	}

	@Override
	public void onError(Throwable throwable) {
		// TODO Called if an error has been encountered while compressing
	}
}).launch(); // Start compression
```



#### 测试信息

CodeCheck代码测试无异常

CloudTest代码测试无异常

病毒安全检测通过

当前版本demo功能与原组件基本无差异

#### 版本迭代

- 1.0.0

#### License
Copyright 2016 Zheng Zibin
```
Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
```