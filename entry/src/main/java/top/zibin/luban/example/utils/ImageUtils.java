/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package top.zibin.luban.example.utils;

import ohos.media.image.ImageSource;
import ohos.media.image.PixelMap;
import ohos.media.image.common.PixelFormat;
import ohos.media.image.common.Rect;
import ohos.media.image.common.Size;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;

import static top.zibin.luban.example.utils.Constants.IO_EXCEPTION;

/**
 * ImageUtils
 */
public class ImageUtils {
    private static final String IMAGE_TYPE = "image/*";

    private static final int IO_END_LEN = -1;

    private static final int CACHE_SIZE = 256 * 1024;

    private static final String LABEL = ImageUtils.class.getSimpleName();

    private ImageUtils() {
        /* Do nothing */
    }

    /**
     * * getPixelMapByUri
     *
     * @param uri uri
     * @return PixelMap pixelMap
     */
    public static PixelMap getPixelMapByUri(String uri) {
        byte[] data = null;
        data = read(uri);

        if (data == null || data.length == 0) {
            return null;
        }

        ImageSource.SourceOptions srcOpts = new ImageSource.SourceOptions();
        srcOpts.formatHint = IMAGE_TYPE;
        ImageSource imageSource = ImageSource.create(data, srcOpts);
        if (imageSource == null) {
            return null;
        }

        ImageSource.DecodingOptions decodingOpts = new ImageSource.DecodingOptions();
        decodingOpts.desiredSize = new Size(0, 0);
        decodingOpts.desiredRegion = new Rect(0, 0, 0, 0);
        decodingOpts.desiredPixelFormat = PixelFormat.ARGB_8888;

        return imageSource.createPixelmap(decodingOpts);
    }

    private static byte[] read(String filePath) {
        FileInputStream fileInputStream = null;
        byte[] cache = new byte[CACHE_SIZE];
        byte[] result = new byte[0];
        int len = IO_END_LEN;
        try (ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream()) {
            fileInputStream = new FileInputStream(new File(filePath));
            len = fileInputStream.read(cache);
            while (len != IO_END_LEN) {
                byteArrayOutputStream.write(cache, 0, len);
                len = fileInputStream.read(cache);
            }
            result = byteArrayOutputStream.toByteArray();
        } catch (IOException e) {
            LogUtil.error(LABEL, IO_EXCEPTION);
        } finally {
            try {
                if (fileInputStream != null) {
                    fileInputStream.close();
                }
            } catch (IOException e) {
                LogUtil.error(LABEL, IO_EXCEPTION);
            }
        }
        return result;
    }
}
