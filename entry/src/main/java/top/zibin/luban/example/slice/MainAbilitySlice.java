/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package top.zibin.luban.example.slice;

import top.zibin.luban.example.ImageAdapter;
import top.zibin.luban.example.ImageBean;
import top.zibin.luban.example.ResourceTable;
import top.zibin.luban.example.utils.Constants;
import top.zibin.luban.example.utils.LogUtil;
import top.zibin.luban.example.utils.Utils;

import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;

import ohos.agp.components.Button;
import ohos.agp.components.ListContainer;
import ohos.agp.window.dialog.ToastDialog;

import ohos.app.Environment;

import ohos.eventhandler.EventHandler;
import ohos.eventhandler.EventRunner;

import ohos.global.resource.NotExistException;
import ohos.global.resource.Resource;
import ohos.global.resource.WrongTypeException;

import top.zibin.luban.Luban;
import top.zibin.luban.OnCompressListener;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import static top.zibin.luban.example.utils.Constants.IO_EXCEPTION;
import static top.zibin.luban.example.utils.Constants.RESOURCE_ERROR;

/**
 * MainAbilitySlice extends AbilitySlice
 */
public class MainAbilitySlice extends AbilitySlice {
    private static final String TAG = MainAbilitySlice.class.getSimpleName();

    private static final int RANGE = 3;

    private static final int SIZE = 100;

    private static final int BUFFER_SIZE = 4096;

    private final List<ImageBean> mImageList = new ArrayList<>();

    private final List<ImageBean> imageBeans = new ArrayList<>();

    private ImageAdapter mAdapter = new ImageAdapter(this, mImageList);

    private List<File> originPhotos = new ArrayList<>();

    private ListContainer listContainer;

    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_main_ability_slice);
        initialiseViews();
    }

    @Override
    public void onActive() {
        super.onActive();
        originPhotos = rawToFiles();
    }

    private void initialiseViews() {
        listContainer = (ListContainer) findComponentById(ResourceTable.Id_imageList);

        Button compressImages = (Button) findComponentById(ResourceTable.Id_compress);
        compressImages.setClickedListener(component -> {
            imageBeans.clear();
            File dataDir = getContext().getExternalFilesDir(Environment.DIRECTORY_PICTURES);
            Luban.with(getContext())
                    .setTargetDir(dataDir.getPath())
                    .ignoreBy(SIZE)
                    .load(originPhotos)
                    .setCompressListener(new OnCompressListener() {
                        @Override
                        public void onStart() {
                        }

                        @Override
                        public void onSuccess(File file) {
                            showResult(originPhotos, file);
                            if (originPhotos.size() == imageBeans.size()) {
                                new EventHandler(EventRunner.getMainEventRunner()).postTask(() -> refreshList());
                            }
                        }

                        @Override
                        public void onError(Throwable throwable) {
                        }
                    }).launch();
            showToast();
        });
    }

    private List<File> rawToFiles() {
        final List<File> files = new ArrayList<>();
        byte[] oriBuff = null;

        int[] resId = {ResourceTable.Media_image_0, ResourceTable.Media_image_1, ResourceTable.Media_image_2};
        for (int count = 0; count < RANGE; count++) {
            File file = new File(getExternalFilesDir(null), "test_" + count + ".jpg");
            try (FileOutputStream fos = new FileOutputStream(file)) {
                try {
                    Resource resource = this.getResourceManager().getResource(resId[count]);
                    oriBuff = readResource(resource);
                } catch (IOException | NotExistException e) {
                    LogUtil.error(TAG, IO_EXCEPTION);
                }

                int offset = 0;
                while (oriBuff.length - offset > 0) {
                    if (oriBuff.length - offset > BUFFER_SIZE) {
                        fos.write(oriBuff, offset, BUFFER_SIZE);
                        offset += BUFFER_SIZE;
                    } else {
                        fos.write(oriBuff, offset, oriBuff.length - offset);
                        offset += oriBuff.length - offset;
                    }
                }
                files.add(file);
            } catch (IOException e) {
                LogUtil.error(TAG, IO_EXCEPTION);
            }
        }
        return files;
    }

    private static byte[] readResource(Resource resource) {
        final int bufferSize = 4 * 1024;
        final int ioEnd = -1;

        ByteArrayOutputStream output = new ByteArrayOutputStream();
        byte[] buffer = new byte[bufferSize];
        while (true) {
            try {
                int readLen = resource.read(buffer, 0, bufferSize);
                if (readLen == ioEnd) {
                    break;
                }
                output.write(buffer, 0, readLen);
            } catch (IOException e) {
                LogUtil.error(TAG, IO_EXCEPTION);
                break;
            } finally {
                try {
                    output.close();
                } catch (IOException e) {
                    LogUtil.error(TAG, IO_EXCEPTION);
                }
            }
        }
        return output.toByteArray();
    }

    private void refreshList() {
        mAdapter = new ImageAdapter(getContext(), imageBeans);
        listContainer.setItemProvider(mAdapter);
    }

    private void showToast() {
        try {
            String message = getContext().getResourceManager().getElement(ResourceTable.String_message).getString();
            ToastDialog toastDialog = new ToastDialog(getContext());
            toastDialog.setText(message);
            toastDialog.show();
        } catch (IOException | NotExistException | WrongTypeException e) {
            LogUtil.error(TAG, RESOURCE_ERROR);
        }
    }

    private void showResult(List<File> photos, File file) {
        int[] originSize = Utils.computeSize(photos.get(mAdapter.getCount()));
        int[] thumbSize = Utils.computeSize(file);
        String originArg = String.format(Locale.CHINA,
                "Original image parameters:%d*%d, %dk", originSize[0],
                originSize[1], photos.get(mAdapter.getCount()).length() >> Constants.MAGIC_10);
        String thumbArg = String.format(Locale.CHINA,
                "Parameters after compression:%d*%d, %dk", thumbSize[0],
                thumbSize[1], file.length() >> Constants.MAGIC_10);
        ImageBean imageBean = null;
        try {
            imageBean = new ImageBean(originArg, thumbArg, file.getCanonicalPath());
        } catch (IOException e) {
            LogUtil.error(TAG, IO_EXCEPTION);
        }
        imageBeans.add(imageBean);
    }
}
