package top.zibin.luban.example;

import top.zibin.luban.example.utils.ImageUtils;

import ohos.agp.components.ComponentContainer;
import ohos.agp.components.Component;
import ohos.agp.components.BaseItemProvider;
import ohos.agp.components.LayoutScatter;
import ohos.agp.components.Text;
import ohos.agp.components.Image;

import ohos.app.Context;

import java.util.List;

/**
 * ImageAdapter
 */
public class ImageAdapter extends BaseItemProvider {
    private final Context context;

    private final List<ImageBean> mData;

    /**
     * ImageAdapter
     *
     * @param context context
     * @param data    List ImageBean
     */
    public ImageAdapter(Context context, List<ImageBean> data) {
        this.context = context;
        mData = data;
    }

    @Override
    public int getCount() {
        if (mData == null || mData.size() == 0) {
            return 0;
        } else {
            return mData.size();
        }
    }

    @Override
    public Object getItem(int index) {
        return mData.get(index);
    }

    @Override
    public long getItemId(int itemId) {
        return itemId;
    }

    @Override
    public Component getComponent(int position, Component convertView, ComponentContainer componentContainer) {
        Component rootComponent = convertView;

        if (rootComponent == null) {
            rootComponent = LayoutScatter.getInstance(context).parse(ResourceTable.Layout_image_adapter, null, false);
        }

        ImageBean imageBean = getImageBean(position);

        Text originalArguments = (Text) rootComponent.findComponentById(ResourceTable.Id_original_arguments);
        if (imageBean.getOriginArg() != null) {
            originalArguments.setText(imageBean.getOriginArg());
        }

        Text compressedArguments = (Text) rootComponent.findComponentById(ResourceTable.Id_compressed_arguments);
        compressedArguments.setText(imageBean.getThumbArg());

        Image compressedImage = (Image) rootComponent.findComponentById(ResourceTable.Id_compressed_image);
        compressedImage.setPixelMap(ImageUtils.getPixelMapByUri(imageBean.getImage()));

        return rootComponent;
    }

    private ImageBean getImageBean(int position) {
        return mData.get(position);
    }
}