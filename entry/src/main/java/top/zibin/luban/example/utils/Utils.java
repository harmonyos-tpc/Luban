/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package top.zibin.luban.example.utils;

import ohos.media.image.ImageSource;
import ohos.media.image.PixelMap;
import ohos.media.image.common.ColorSpace;
import ohos.media.image.common.PixelFormat;
import ohos.media.image.common.Rect;
import ohos.media.image.common.Size;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;

import static top.zibin.luban.example.utils.Constants.IO_EXCEPTION;

/**
 * Utils
 */
public class Utils {
    private static final String TAG = Utils.class.getSimpleName();

    /**
     * computeSize method file name
     *
     * @param srcImg File
     * @return int integer
     */
    public static int[] computeSize(File srcImg) {
        int[] size = new int[Constants.MAGIC_2];
        try (InputStream is = new FileInputStream(srcImg)) {
            ImageSource.DecodingOptions decodingOpts = new ImageSource.DecodingOptions();
            resetOptions(decodingOpts);
            PixelMap pixelmap = decodePixelMap(is, decodingOpts);
            size[0] = pixelmap.getImageInfo().size.width;
            size[1] = pixelmap.getImageInfo().size.height;
        } catch (IOException e) {
            LogUtil.error(TAG, IO_EXCEPTION);
        }
        return size;
    }

    private static void resetOptions(ImageSource.DecodingOptions decodingOpts) {
        decodingOpts.desiredSize = new Size(0, 0);
        decodingOpts.desiredRegion = new Rect(0, 0, 0, 0);
        decodingOpts.desiredPixelFormat = PixelFormat.ARGB_8888;
        decodingOpts.allowPartialImage = false;
        decodingOpts.desiredColorSpace = ColorSpace.UNKNOWN;
        decodingOpts.rotateDegrees = 0.0F;
        decodingOpts.sampleSize = 1;
    }

    private static PixelMap decodePixelMap(InputStream is, ImageSource.DecodingOptions options)
            throws IOException {
        ImageSource.SourceOptions srcOpts = new ImageSource.SourceOptions();
        srcOpts.formatHint = null;
        ImageSource imageSource = ImageSource.create(is, srcOpts);

        if (imageSource == null) {
            throw new FileNotFoundException();
        }
        return imageSource.createPixelmap(options);
    }
}

