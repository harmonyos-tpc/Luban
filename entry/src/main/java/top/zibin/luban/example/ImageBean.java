package top.zibin.luban.example;

/**
 * ImageBean
 */
public class ImageBean {
    private String originArg;

    private String thumbArg;

    private String image;

    /**
     * ImageBean method
     *
     * @param originArg String
     * @param thumbArg  String
     * @param image     String
     */
    public ImageBean(String originArg, String thumbArg, String image) {
        this.originArg = originArg;
        this.thumbArg = thumbArg;
        this.image = image;
    }

    /**
     * getOriginArg
     *
     * @return originArg String
     */
    String getOriginArg() {
        return originArg;
    }

    /**
     * setOriginArg
     *
     * @param originArg String
     */
    public void setOriginArg(String originArg) {
        this.originArg = originArg;
    }

    /**
     * getThumbArg
     *
     * @return thumbArg String
     */
    String getThumbArg() {
        return thumbArg;
    }

    /**
     * setThumbArg
     *
     * @param thumbArg String
     */
    public void setThumbArg(String thumbArg) {
        this.thumbArg = thumbArg;
    }

    /**
     * getImage
     *
     * @return image String
     */
    String getImage() {
        return image;
    }

    /**
     * setImage
     *
     * @param image String
     */
    public void setImage(String image) {
        this.image = image;
    }
}
