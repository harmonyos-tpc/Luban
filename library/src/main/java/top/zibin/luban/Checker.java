package top.zibin.luban;

import ohos.media.image.ImageSource;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;

import java.util.Arrays;

import static top.zibin.luban.Constants.IO_EXCEPTION;

/**
 * Checker enum
 */
enum Checker {
    SINGLE;

    private static final String TAG = "Luban";

    private static final String JPG = ".jpg";

    private final byte[] jpegSignature = new byte[]{(byte) 0xFF, (byte) 0xD8, (byte) 0xFF};

    /**
     * Returns the getOrientation in clockwise. Values are 0, 90, 180, or 270.
     *
     * @param is InputStream
     * @return orientation
     */
    int getOrientation(InputStream is) {
        return getOrientation(toByteArray(is));
    }

    /**
     * getOrientation if it is JPG.
     *
     * @param jpeg byte[]
     * @return int value
     */
    private int getOrientation(byte[] jpeg) {
        if (jpeg == null) {
            return 0;
        }
        int offset = 0;
        int length = 0;
        while (jpeg.length > offset + Constants.MAGIC_3
                && (jpeg[offset++] & Constants.MAGIC_HEX) == Constants.MAGIC_HEX) {
            int marker = jpeg[offset] & Constants.MAGIC_HEX;
            if (marker == Constants.MAGIC_HEX) {
                continue;
            }
            offset++;
            if (marker == Constants.MAGIC_HEX_VAL_EIGHT || marker == 0x01) {
                continue;
            }
            if (marker == Constants.MAGIC_HEX_VAL || marker == Constants.MAGIC_HEX_VAL_A) {
                break;
            }
            length = pack(jpeg, offset, Constants.MAGIC_2, false);
            if (length < Constants.MAGIC_2 || offset + length > jpeg.length) {
                return 0;
            }
            if (marker == Constants.MAGIC_HEX_E_ONE && length >= Constants.MAGIC_8
                    && pack(jpeg, offset + Constants.MAGIC_2, Constants.MAGIC_4, false) == Constants.MAGIC_HEX_SIXTY_SIX
                    && pack(jpeg, offset + Constants.MAGIC_6, Constants.MAGIC_2, false) == 0) {
                offset += Constants.MAGIC_8;
                length -= Constants.MAGIC_8;
                break;
            }
            offset += length;
            length = 0;
        }
        if (length > Constants.MAGIC_8) {
            return updateOffsetAndLength(length, offset, jpeg);
        }
        return 0;
    }

    private int updateOffsetAndLength(int length, int offset, byte[] jpeg) {
        int updateLength = length;
        int updateOffset = offset;
        int tag = pack(jpeg, updateOffset, Constants.MAGIC_4, false);
        if (tag != Constants.MAGIC_HEX_AOO && tag != Constants.MAGIC_HEX_TWO_A) {
            return 0;
        }
        boolean littleEndian = (tag == Constants.MAGIC_HEX_AOO);
        int count = pack(jpeg, updateOffset + Constants.MAGIC_4, Constants.MAGIC_4, littleEndian) + Constants.MAGIC_2;
        if (count < Constants.MAGIC_10 || count > updateLength) {
            return 0;
        }
        updateOffset += count;
        updateLength -= count;
        count = pack(jpeg, updateOffset - Constants.MAGIC_2, Constants.MAGIC_2, littleEndian);
        while (count-- > 0 && updateLength >= Constants.MAGIC_12) {
            tag = pack(jpeg, updateOffset, Constants.MAGIC_2, littleEndian);
            if (tag == Constants.MAGIC_HEX_TWELVE) {
                int orientation = pack(jpeg, updateOffset + Constants.MAGIC_8, Constants.MAGIC_2, littleEndian);
                switch (orientation) {
                    case 1:
                        return 0;
                    case Constants.MAGIC_3:
                        return Constants.MAGIC_180;
                    case Constants.MAGIC_6:
                        return Constants.MAGIC_90;
                    case Constants.MAGIC_8:
                        return Constants.MAGIC_270;
                    default:
                }
                return 0;
            }
            updateOffset += Constants.MAGIC_12;
            updateLength -= Constants.MAGIC_12;
        }
        return 0;
    }

    /**
     * jpgSignature if it is JPG.
     *
     * @param is image file input stream
     * @return boolean
     */
    boolean jpgSignature(InputStream is) {
        return jpgSignature(toByteArray(is));
    }

    /**
     * jpgSignature if it is JPG.
     *
     * @param data byte[]
     * @return true
     */
    private boolean jpgSignature(byte[] data) {
        if (data == null || data.length < Constants.MAGIC_3) {
            return false;
        }
        byte[] signatureB = new byte[]{data[0], data[1], data[Constants.MAGIC_2]};
        return Arrays.equals(jpegSignature, signatureB);
    }

    /**
     * jpgSignature if it is JPG.
     *
     * @param input InputStreamProvider
     * @return String
     */
    String extSuffix(InputStreamProvider input) {
        try {
            ImageSource.SourceOptions srcOpts = new ImageSource.SourceOptions();
            ImageSource imageSource = ImageSource.create(input.open(), srcOpts);

            input.close();
            return imageSource.getSourceInfo().encodedFormat.replace("image/", ".");
        } catch (IOException e) {
            return JPG;
        }
    }

    /**
     * needCompress if it is JPG.
     *
     * @param leastCompressSize int
     * @param path String
     * @return true
     */
    boolean needCompress(int leastCompressSize, String path) {
        if (leastCompressSize > 0) {
            File source = new File(path);
            return source.exists() && source.length() > (leastCompressSize << Constants.MAGIC_10);
        }
        return true;
    }

    private int pack(byte[] bytes, int offset, int length, boolean isLittleEndian) {
        int packOffset = offset;
        int packLength = length;
        int step = 1;
        if (isLittleEndian) {
            packOffset += packLength - 1;
            step = Constants.MAGIC_MINUS_ONE;
        }

        int value = 0;
        while (packLength-- > 0) {
            value = (value << Constants.MAGIC_8) | (bytes[packOffset] & Constants.MAGIC_HEX);
            packOffset += step;
        }
        return value;
    }

    private byte[] toByteArray(InputStream is) {
        if (is == null) {
            return new byte[0];
        }
        ByteArrayOutputStream buffer = new ByteArrayOutputStream();
        int read;
        byte[] data = new byte[Constants.MAGIC_4096];
        try {
            while ((read = is.read(data, 0, data.length)) != Constants.MAGIC_MINUS_ONE) {
                buffer.write(data, 0, read);
            }
        } catch (IOException ignored) {
            return new byte[0];
        } finally {
            try {
                buffer.close();
            } catch (IOException exception) {
                LogUtil.info(TAG, IO_EXCEPTION);
            }
        }
        return buffer.toByteArray();
    }
}
