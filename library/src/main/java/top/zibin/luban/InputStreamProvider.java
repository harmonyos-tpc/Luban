package top.zibin.luban;

import java.io.IOException;
import java.io.InputStream;

/**
 * 通过此接口获取输入流，以兼容文件、FileProvider方式获取到的图片
 * <p>
 * Get the input stream through this interface, and obtain the picture using compatible files and FileProvider
 */
public interface InputStreamProvider {
    /**
     * Function description
     * open method
     *
     * @return inputstream
     * @throws IOException exception
     */
    InputStream open() throws IOException;

    /**
     * Function description
     * close method
     */
    void close();

    /**
     * Function description
     * getPath method
     *
     * @return string
     */
    String getPath();
}
