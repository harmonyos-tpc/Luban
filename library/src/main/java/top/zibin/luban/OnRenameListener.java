package top.zibin.luban;

/**
 * Author: zibin
 * Datetime: 2018/5/18
 * <p>
 * 提供修改压缩图片命名接口
 * <p>
 * A functional interface (callback) that used to rename the file after compress.
 */
public interface OnRenameListener {
    /**
     * <p>
     * Call before compression begins.
     *
     * @param filePath file path
     * @return file name
     */
    String rename(String filePath);
}
