/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package top.zibin.luban;

/**
 * Function description
 * class Constants
 */
class Constants {
    /**
     * BUNDLE_NAME
     */
    static final String IO_EXCEPTION = "IO_EXCEPTION";

    /**
     * MAGIC_2 ID
     */
    static final int MAGIC_2 = 2;

    /**
     * MAGIC_10 ID
     */
    static final int MAGIC_10 = 10;

    /**
     * MAGIC_100 ID
     */
    static final int MAGIC_100 = 100;

    /**
     * MAGIC_1000 ID
     */
    static final double MAGIC_1000 = 1000;

    /**
     * MAGIC_4096 ID
     */
    static final int MAGIC_4096 = 4096;

    /**
     * MAGIC_MINUS_ONE ID
     */
    static final int MAGIC_MINUS_ONE = -1;

    /**
     * MAGIC_8 ID
     */
    static final int MAGIC_8 = 8;

    /**
     * MAGIC_8 ID
     */
    static final int MAGIC_HEX = 0XFF;

    /**
     * MAGIC_3 ID
     */
    static final int MAGIC_3 = 3;

    /**
     * MAGIC_HEX_VAL ID
     */
    static final int MAGIC_HEX_VAL = 0xD9;

    /**
     * MAGIC_HEX_VAL ID
     */
    static final int MAGIC_HEX_VAL_EIGHT = 0xD8;

    /**
     * MAGIC_HEX_VAL ID
     */
    static final int MAGIC_HEX_VAL_A = 0xDA;

    /**
     * MAGIC_4 ID
     */
    static final int MAGIC_4 = 4;

    /**
     * MAGIC_4 ID
     */
    static final int MAGIC_12 = 12;

    /**
     * MAGIC_270 ID
     */
    static final int MAGIC_270 = 270;

    /**
     * MAGIC_90 ID
     */
    static final int MAGIC_90 = 90;

    /**
     * MAGIC_180 ID
     */
    static final int MAGIC_180 = 180;

    /**
     * MAGIC_HEX_TWELVE ID
     */
    static final int MAGIC_HEX_TWELVE = 0x0112;

    /**
     * MAGIC_6 ID
     */
    static final int MAGIC_6 = 6;

    /**
     * MAGIC_HEX_AOO ID
     */
    static final int MAGIC_HEX_AOO = 0x49492A00;

    /**
     * MAGIC_HEX_TWO_A ID
     */
    static final int MAGIC_HEX_TWO_A = 0x4D4D002A;

    /**
     * MAGIC_HEX_SIXTY_SIX ID
     */
    static final int MAGIC_HEX_SIXTY_SIX = 0x45786966;

    /**
     * MAGIC_HEX_EONE ID
     */
    static final int MAGIC_HEX_E_ONE = 0xE1;

    /**
     * MAGIC_60 ID
     */
    static final int MAGIC_60 = 60;

    /**
     * MAGIC_1280 ID
     */
    static final int MAGIC_1280 = 1280;

    /**
     * MAGIC_1280 ID
     */
    static final int MAGIC_4990 = 4990;

    /**
     * MAGIC_10240 ID
     */
    static final int MAGIC_10240 = 10240;

    /**
     * MAGIC_10240 ID
     */
    static final int MAGIC_1664 = 1664;

    /**
     * MAGIC_POINT5625 ID
     */
    static final float MAGIC_POINT5625 = 0.5625f;

    /**
     * MAGIC_POINT5 ID
     */
    static final float MAGIC_POINT5 = 0.5f;

    /**
     * IMAGE_PNG
     */
    static final String IMAGE_PNG = "image/*";

    private Constants() {
        /* Do nothing */
    }
}
