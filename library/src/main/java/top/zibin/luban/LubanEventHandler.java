/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package top.zibin.luban;

import ohos.eventhandler.EventHandler;
import ohos.eventhandler.EventRunner;
import ohos.eventhandler.InnerEvent;

/**
 * Function description
 * class LubanEventHandler
 */
public class LubanEventHandler extends EventHandler {
    private final Callback mCallback;

    /**
     * constructor
     *
     * @param runner EventRunner
     * @param callback Callback
     * @throws IllegalArgumentException exception
     */
    public LubanEventHandler(EventRunner runner, Callback callback) throws IllegalArgumentException {
        super(runner);
        mCallback = callback;
    }

    /**
     * Function description
     * interface Callback
     */
    public interface Callback {
        /**
         * Function description
         * processEvent
         *
         * @param event InnerEvent
         * @return processevent
         */
        boolean processEvent(InnerEvent event);
    }

    /**
     * Function description
     * processEvent
     *
     * @param event InnerEvent
     */
    public void processEvent(InnerEvent event) {
        if (mCallback != null) {
            if (mCallback.processEvent(event)) {
                return;
            }
        }
        super.processEvent(event);
    }
}
