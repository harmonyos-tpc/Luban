package top.zibin.luban;

import java.io.File;

/**
 * Function description
 * interface OnCompressListener
 */
public interface OnCompressListener {
    /**
     * Fired when the compression is started, override to handle in your own code
     * onStart
     */
    void onStart();

    /**
     * Fired when a compression returns successfully, override to handle in your own code
     * onSuccess
     *
     * @param file File
     */
    void onSuccess(File file);

    /**
     * Fired when a compression fails to complete, override to handle in your own code
     * onError
     *
     * @param throwable File
     */
    void onError(Throwable throwable);
}
