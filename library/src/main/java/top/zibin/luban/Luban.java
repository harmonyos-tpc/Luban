package top.zibin.luban;

import ohos.app.Context;
import ohos.app.dispatcher.TaskDispatcher;
import ohos.app.dispatcher.task.Revocable;
import ohos.app.dispatcher.task.TaskPriority;

import ohos.eventhandler.EventRunner;
import ohos.eventhandler.InnerEvent;

import ohos.utils.net.Uri;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import static top.zibin.luban.Constants.IO_EXCEPTION;

/**
 * Function description
 * Luban implement LubanEventHandler
 */
public class Luban implements LubanEventHandler.Callback {
    private static final String TAG = Luban.class.getSimpleName();

    private static final String DEFAULT_DISK_CACHE_DIR = "luban_disk_cache";

    private static final int MSG_COMPRESS_SUCCESS = 0;

    private static final int MSG_COMPRESS_START = 1;

    private static final int MSG_COMPRESS_ERROR = 2;

    private String mTargetDir;

    private boolean isFocusAlpha;

    private int mLeastCompressSize;

    private OnRenameListener mRenameListener;

    private OnCompressListener mCompressListener;

    private CompressionPredicate mCompressionPredicate;

    private List<InputStreamProvider> mStreamProviders;

    private TaskDispatcher globalTaskDispatcher;

    private LubanEventHandler mHandler;

    private Luban(Builder builder) {
        this.mTargetDir = builder.mTargetDir;
        this.mRenameListener = builder.mRenameListener;
        this.mStreamProviders = builder.mStreamProviders;
        this.mCompressListener = builder.mCompressListener;
        this.mLeastCompressSize = builder.mLeastCompressSize;
        this.mCompressionPredicate = builder.mCompressionPredicate;
        this.globalTaskDispatcher = builder.globalTaskDispatcher;
        mHandler = new LubanEventHandler(EventRunner.getMainEventRunner(), this);
    }

    /**
     * getImageCustomFile name
     *
     * @param context Context
     * @return Builder Builder
     */
    public static Builder with(Context context) {
        return new Builder(context);
    }

    /**
     * getImageCacheFile name
     *
     * @param context A context.
     * @param suffix  String
     * @return file File
     */
    private File getImageCacheFile(Context context, String suffix) {
        if (this.mTargetDir == null) {
            mTargetDir = getImageCacheDir(context).getAbsolutePath();
        }
        String cacheBuilder = mTargetDir + "/" + System.currentTimeMillis()
                + (int) (Math.random() * Constants.MAGIC_1000) + ((suffix == null) ? ".jpg" : suffix);
        return new File(cacheBuilder);
    }

    /**
     * getImageCustomFile name
     *
     * @param context  A context.
     * @param filename String
     * @return file File
     */
    private File getImageCustomFile(Context context, String filename) {
        if (mTargetDir == null) {
            mTargetDir = getImageCacheDir(context).getAbsolutePath();
        }
        String cacheBuilder = mTargetDir + "/" + filename;
        return new File(cacheBuilder);
    }

    /**
     * getImageCacheDir to store retrieved audio.
     *
     * @param context A context.
     * @return File
     * @see #getImageCacheDir(Context, String)
     */
    private File getImageCacheDir(Context context) {
        return getImageCacheDir(context, DEFAULT_DISK_CACHE_DIR);
    }

    /**
     * getImageCacheDir to store retrieved media and thumbnails.
     *
     * @param context   A context.
     * @param cacheName The name of the subdirectory in which to store the cache.
     * @return File
     * @see #getImageCacheDir(Context)
     */
    private static File getImageCacheDir(Context context, String cacheName) {
        File cacheDir = context.getExternalCacheDir();
        if (cacheDir != null) {
            File result = new File(cacheDir, cacheName);
            if (!result.mkdirs() && (!result.exists() || !result.isDirectory())) {
                return null;
            }
            return result;
        }
        return null;
    }

    private void launch(final Context context) {
        if (mStreamProviders == null || mStreamProviders.size() == 0 && mCompressListener != null) {
            mCompressListener.onError(new NullPointerException("image file cannot be null"));
        }

        if (globalTaskDispatcher == null) {
            globalTaskDispatcher = context.getGlobalTaskDispatcher(TaskPriority.DEFAULT);
        }

        Iterator<InputStreamProvider> iterator = mStreamProviders.iterator();

        while (iterator.hasNext()) {
            final InputStreamProvider path = iterator.next();

            Revocable revocable = globalTaskDispatcher.asyncDispatch(() -> {
                try {
                    mHandler.processEvent(InnerEvent.get(MSG_COMPRESS_START, 0, null));

                    File result = compress(context, path);
                    mHandler.processEvent(InnerEvent.get(MSG_COMPRESS_SUCCESS, 0, result));
                } catch (IOException e) {
                    mHandler.processEvent(InnerEvent.get(MSG_COMPRESS_ERROR, 0, e.getMessage()));
                }
            });
            iterator.remove();
        }
    }

    /**
     * start get and return the file
     *
     * @param input   InputStreamProvider
     * @param context Context
     * @return File
     * @throws IOException IOException is thrown
     */
    private File get(InputStreamProvider input, Context context) throws IOException {
        try {
            return new Engine(input, getImageCacheFile(context, Checker.SINGLE.extSuffix(input)),
                    isFocusAlpha).compress();
        } finally {
            input.close();
        }
    }

    private List<File> get(Context context) throws IOException {
        List<File> results = new ArrayList<>();
        Iterator<InputStreamProvider> iterator = mStreamProviders.iterator();

        while (iterator.hasNext()) {
            results.add(compress(context, iterator.next()));
            iterator.remove();
        }

        return results;
    }

    private File compress(Context context, InputStreamProvider path) throws IOException {
        try {
            return compressReal(context, path);
        } finally {
            path.close();
        }
    }

    private File compressReal(Context context, InputStreamProvider path) throws IOException {
        File result = null;

        File outFile = getImageCacheFile(context, Checker.SINGLE.extSuffix(path));

        if (mRenameListener != null) {
            String filename = mRenameListener.rename(path.getPath());
            outFile = getImageCustomFile(context, filename);
        }

        if (mCompressionPredicate == null) {
            result = Checker.SINGLE.needCompress(mLeastCompressSize, path.getPath()) ? new Engine(path, outFile,
                    isFocusAlpha).compress() : new File(path.getPath());
        }
        return result;
    }

    @Override
    public boolean processEvent(InnerEvent event) {
        if (mCompressListener == null) {
            return false;
        }

        switch (event.eventId) {
            case MSG_COMPRESS_START:
                mCompressListener.onStart();
                break;
            case MSG_COMPRESS_SUCCESS:
                mCompressListener.onSuccess((File) event.object);
                break;
            case MSG_COMPRESS_ERROR:
                mCompressListener.onError((Throwable) event.object);
                break;
            default:
        }
        return false;
    }

    /**
     * Function description
     * Builder class
     */
    public static class Builder {
        private Context context;

        private String mTargetDir;

        private boolean isFocusAlpha;

        private int mLeastCompressSize = Constants.MAGIC_100;

        private OnRenameListener mRenameListener;

        private OnCompressListener mCompressListener;

        private CompressionPredicate mCompressionPredicate;

        private List<InputStreamProvider> mStreamProviders;

        private TaskDispatcher globalTaskDispatcher;

        /**
         * constructor
         *
         * @param context context
         */
        Builder(Context context) {
            this.context = context;
            this.mStreamProviders = new ArrayList<>();
        }

        private Luban build() {
            return new Luban(this);
        }

        /**
         * load
         *
         * @param inputStreamProvider InputStreamProvider
         * @return Builder
         */
        public Builder load(InputStreamProvider inputStreamProvider) {
            mStreamProviders.add(inputStreamProvider);
            return this;
        }

        /**
         * load
         *
         * @param file File
         * @return Builder
         */
        public Builder load(final File file) {
            mStreamProviders.add(new InputStreamAdapter() {
                @Override
                public InputStream openInternal() throws IOException {
                    return new FileInputStream(file);
                }

                @Override
                public String getPath() {
                    String path = null;
                    try {
                        path = file.getCanonicalPath();
                    } catch (IOException e) {
                        LogUtil.error(TAG, IO_EXCEPTION);
                    }
                    return path;
                }
            });
            return this;
        }

        /**
         * load
         *
         * @param string String
         * @return Builder
         */
        public Builder load(final String string) {
            mStreamProviders.add(new InputStreamAdapter() {
                @Override
                public InputStream openInternal() throws IOException {
                    return new FileInputStream(string);
                }

                @Override
                public String getPath() {
                    return string;
                }
            });
            return this;
        }

        /**
         * load
         *
         * @param list list generics
         * @param <T>  generics
         * @return Builder
         */
        public <T> Builder load(List<?> list) {
            for (Object src : list) {
                if (src instanceof String) {
                    load((String) src);
                } else if (src instanceof File) {
                    load((File) src);
                } else if (src instanceof Uri) {
                    load((Uri) src);
                } else {
                    throw new IllegalArgumentException(
                            "Incoming data type exception, it must be String, File, Uri or Bitmap");
                }
            }
            return this;
        }

        /**
         * load File method
         *
         * @param uri Uri
         * @return file
         */
        public Builder load(final Uri uri) {
            mStreamProviders.add(new InputStreamAdapter() {
                @Override
                public InputStream openInternal() {
                    return null;
                }

                @Override
                public String getPath() {
                    return uri.getDecodedPath();
                }
            });
            return this;
        }

        /**
         * putGear
         *
         * @param gear integer value
         * @return Builder
         */
        public Builder putGear(int gear) {
            return this;
        }

        /**
         * setRenameListener
         *
         * @param listener OnRenameListener value
         * @return Builder
         */
        public Builder setRenameListener(OnRenameListener listener) {
            this.mRenameListener = listener;
            return this;
        }

        /**
         * setCompressListener
         *
         * @param listener OnCompressListener value
         * @return Builder
         */
        public Builder setCompressListener(OnCompressListener listener) {
            this.mCompressListener = listener;
            return this;
        }

        /**
         * setTargetDir
         *
         * @param targetDir String value
         * @return Builder
         */
        public Builder setTargetDir(String targetDir) {
            this.mTargetDir = targetDir;
            return this;
        }

        /**
         * Do I need to keep the image's alpha channel
         *
         * @param isFocusable <p> true - to keep alpha channel, the compress speed will be slow. </p>
         *                   <p> false - don't keep alpha channel, it might have a black background.</p>
         * @return object Builder
         */
        public Builder setFocusAlpha(boolean isFocusable) {
            this.isFocusAlpha = isFocusable;
            return this;
        }

        /**
         * do not compress when the origin image file size less than one value
         *
         * @param size the value of file size, unit KB, default 100K
         * @return Builder
         */
        public Builder ignoreBy(int size) {
            this.mLeastCompressSize = size;
            return this;
        }

        /**
         * do compress image when return value was true, otherwise, do not compress the image file
         *
         * @param compressionPredicate A predicate callback that returns true or false for the given input path should be compressed.
         * @return Builder builder
         */
        public Builder filter(CompressionPredicate compressionPredicate) {
            this.mCompressionPredicate = compressionPredicate;
            return this;
        }

        /**
         * begin compress image with asynchronous
         */
        public void launch() {
            build().launch(context);
        }

        /**
         * get File method
         *
         * @param path String
         * @return file
         * @throws IOException Exception
         */
        public File get(final String path) throws IOException {
            return build().get(new InputStreamAdapter() {
                @Override
                public InputStream openInternal() throws IOException {
                    return new FileInputStream(path);
                }

                @Override
                public String getPath() {
                    return path;
                }
            }, context);
        }

        /**
         * begin get image with synchronize
         *
         * @param mainAbilitySlice Context
         * @return build context
         * @throws IOException Exception
         */
        public List<File> get(Context mainAbilitySlice) throws IOException {
            return build().get(context);
        }
    }
}