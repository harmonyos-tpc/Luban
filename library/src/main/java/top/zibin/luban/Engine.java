package top.zibin.luban;

import ohos.media.image.ImagePacker;
import ohos.media.image.ImageSource;
import ohos.media.image.PixelMap;
import ohos.media.image.common.PixelFormat;
import ohos.media.image.common.Rect;
import ohos.media.image.common.Size;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

/**
 * Responsible for starting compress and managing active and cached resources.
 */
public class Engine {
    private final InputStreamProvider srcImg;

    private final File tagImg;

    private final PixelMap pxlMap;

    private final boolean isFocusAlpha;

    private int srcWidth = 1;

    private int srcHeight = 1;

    /**
     * constructor.
     *
     * @param srcImg InputStreamProvider
     * @param tagImg File
     * @param isFocusAlpha boolean
     */
    Engine(InputStreamProvider srcImg, File tagImg, boolean isFocusAlpha) throws IOException {
        this.tagImg = tagImg;
        this.srcImg = srcImg;
        this.isFocusAlpha = isFocusAlpha;
        ImageSource.SourceOptions opts = new ImageSource.SourceOptions();
        opts.formatHint = Constants.IMAGE_PNG;
        ImageSource imageSource = ImageSource.create(srcImg.open(), opts);
        ImageSource.DecodingOptions decodingOpts = new ImageSource.DecodingOptions();
        decodingOpts.desiredSize = new Size(0, 0);
        decodingOpts.desiredRegion = new Rect(0, 0, 0, 0);
        decodingOpts.desiredPixelFormat = PixelFormat.ARGB_8888;
        decodingOpts.sampleSize = computeSize();

        this.pxlMap = imageSource.createPixelmap(decodingOpts);
        this.srcWidth = imageSource.getImageInfo().size.width;
        this.srcHeight = imageSource.getImageInfo().size.height;
        srcImg.close();
    }

    private int computeSize() {
        srcWidth = srcWidth % Constants.MAGIC_2 == 1 ? srcWidth + 1 : srcWidth;
        srcHeight = srcHeight % Constants.MAGIC_2 == 1 ? srcHeight + 1 : srcHeight;
        int longSide = Math.max(srcWidth, srcHeight);
        int shortSide = Math.min(srcWidth, srcHeight);

        float scale = (float) shortSide / longSide;
        if (scale <= 1 && scale > Constants.MAGIC_POINT5625) {
            if (longSide < Constants.MAGIC_1664) {
                return 1;
            } else if (longSide < Constants.MAGIC_4990) {
                return Constants.MAGIC_2;
            } else if (longSide > Constants.MAGIC_4990 && longSide < Constants.MAGIC_10240) {
                return Constants.MAGIC_4;
            } else {
                return longSide / Constants.MAGIC_1280 == 0 ? 1 : longSide / Constants.MAGIC_1280;
            }
        } else if (scale <= Constants.MAGIC_POINT5625 && scale > Constants.MAGIC_POINT5) {
            return longSide / Constants.MAGIC_1280 == 0 ? 1 : longSide / Constants.MAGIC_1280;
        } else {
            return (int) Math.ceil(longSide / (Constants.MAGIC_1280 / scale));
        }
    }

    /**
     * Function description
     * rotatingImage
     *
     * @param bitmap Bitmap
     * @param angle  Angle
     * @return PixelMap
     */
    private PixelMap rotatingImage(PixelMap bitmap, int angle) {
        return bitmap;
    }

    /**
     * compress
     *
     * @return File
     * @throws IOException exception
     */
    File compress() throws IOException {
        ImagePacker imgPacker = ImagePacker.create();
        ImagePacker.PackingOptions packingOpts = new ImagePacker.PackingOptions();
        packingOpts.quality = Constants.MAGIC_60;
        FileOutputStream outputStream = new FileOutputStream(tagImg);
        imgPacker.initializePacking(outputStream, packingOpts);
        imgPacker.addImage(pxlMap);
        imgPacker.finalizePacking();
        return tagImg;
    }
}