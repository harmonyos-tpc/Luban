# Luban

#### Project introduction 

- entry name ：Luban
- classification ：openharmony的第三方组件适配移植
- function ：An image compressing tool for OHOS with efficiency very close to that of WeChat Moments.This module helps to compress images of type : png, jpg, bitmap and gif.
- Project migration status ：Main function complete 
- Call difference ：nothing 
- Baseline version ：turbo-1.0.0
- Development version ：SDK ver 2.1.0.13，IDE Ver 2.1.1.201004

#### Installation Instructions

1.For using Luban module in sample application,include the below library dependency to generate hap/library.har.

Modify entry build.gradle as below :

```
dependencies {
    implementation project(path: ':library')
}
```

2.For using Luban in separate application, make sure to add "library.har" in libs folder of "entry" module.

Modify entry build.gradle as below :

```
dependencies {
    implementation fileTree(dir: 'libs', include: ['*.har'])
}
```

3.For using Luban from remote repository in separate application, add the below dependency in "entry" build.gradle.

Modify entry build.gradle as below :

```
dependencies {
  implementation 'io.openharmony.tpc.thirdlib:luban:1.0.0'
}
```

#### Usage Instructions

1.Luban internally uses the IO thread to perform image compression, implementations only need to specify what happens when the process finishes successfully.

```java
Luban.with(this)
	.setTargetDir(/*PATH*/) // pass target data directory path
	.ignoreBy(/*SIZE*/)		// pass least compress size
	.load(/*FILE*/) 		// pass image to be compressed
	.setCompressListener(new OnCompressListener() { // Set up return
		@Override
		public void onStart() {
			// TODO Called when compression starts, display loading UI here
		}

	@Override
	public void onSuccess(File file) {
		// TODO Called when compression finishes successfully, provides compressed image
	}

	@Override
	public void onError(Throwable throwable) {
		// TODO Called if an error has been encountered while compressing
	}
}).launch(); // Start compression
```



#### Test information 

CodeCheck pass

CloudTest pass

Virus security detection pass

The demo function of the current version is basically the same as that of the original component 

#### Version iteration 

- 1.0.0

#### License

Copyright 2016 Zheng Zibin

```
Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
```
